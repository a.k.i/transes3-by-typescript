
const path = require('path');
const glob = require('glob');

const entry = {}
glob.sync("./src/*.js").map(function(file){
    entry[file.split("/")[2]] = file;
})

module.exports = {
    entry: entry,

    mode : 'development',

    output: {
        filename: '[name]',
        path: path.resolve(__dirname,'dist')
    },

    module: {
        rules: [
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                use: [
                    "awesome-typescript-loader"
                ]
            }
        ]
    }
}
